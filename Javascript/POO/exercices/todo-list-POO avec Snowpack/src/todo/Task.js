export default class Task {

    _label;

    constructor(label){
        this._label = label;
    }

    set label(value){
        this._label = value;
    }

    get label(){
        return this._label;
    }
}