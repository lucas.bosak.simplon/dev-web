# Jeu de combat : POO & PDO
**Cahier des charges** : On veut créer un jeu où chaque joueur peut se créer un personnage ou sélectionner un personnage existant, parmi 4 grandes familles de guerriers : les mages, les brutes, les archers et les sabreurs.
Chaque personnage apparait avec toute sa vie et des caractéristiques définies. Puis, au cours de ses combats, il va perdre ou gagner de la vie, de la force, de la résistance, et devenir un guerrier redoutable ... ou mourir !


Jeu de combat : https://framindmap.org/c/maps/1318493/public

## ACTIVITÉ 1
  1. Créer la classe Personnage
    - Quels attributs ?
    - Quelles méthodes ?
    - Comment est mon perso lors de sa création ?
    - Y a-t-il des constantes de classe ?
  2. Créer les classes enfants pour chacun des types de personnages.
    - Mêmes questions

## ACTIVITÉ 2

0. Créer le MCD du jeu
  - de quoi veux-je garder une trace dans ma base de donnée ?

1. Création de la base de données pour notre jeu.

  - créer une base, un utilisateur, la table, les colonnes.

2. Connexion à cette base de données

Cours :
PDO, CRUD, SoC, Pattern
Création du code de connexion.
compréhension de la classe PDO.
compréhension du try catch

activité : se connecter à la base de donnée, et faire un code pour enregistrer un nouveau Personnage

## ACTIVITÉ 3

1. Créer une classe Database
  - Elle a pour constantes de classe les paramètres nécessaires à la connexion à la BDD.
  - Elle a des méthodes statiques :
    - retourner tous les personnages stockés dans la BDD
    - initialiser la BDD, si la table n'existe pas (création)
    - Supprimer la table si on supprime le projet (désinstallation)

2. Créer la classe PersonnageRepository
  - Ses méthodes sont le CRUD :
    - créer un nouveau perso
    - Récupérer les caractéristiques d'un perso
    - modifier les persos en fonction des actions
    - supprimer un perso mort
  - lors de son instanciation, appeler la classe Database pour créer une connexion.


## ACTIVITÉ 4

1. Mettre en page le jeu
  - Créer un formulaire qui permet de créer un nouveau perso ou d'en sélectionner un déjà existant, pour poursuivre la partie.
  - Afficher en grille tous les persos, avec le sien au-dessus, tel que :

```
        MOI
LUI LUI LUI LUI LUI
LUI LUI LUI LUI LUI
LUI LUI LUI LUI LUI
        ...

Ceci est un wireframe ;)
```

 -Sa carte personnage contiendra son nom, une barre de vie, sa force et sa résistance, tel que :

```
  /////////////////
  //  NOM PERSO  //
  //   --vie--   //
  //             //
  // force|rési  //
  //    75|18    //
  /////////////////

  Ceci est un wireframe ;)
```

  Pour faire cette grille on pourra faire appel à une classe JS, qui dessinera ainsi chaque perso de la même manière.

  - On fera une classe parente qui dessine un perso, et des classes enfants qui dessinent les différents types de perso avec des caractéristiques propres (mage avec un fond mauve, brute rouge, archers vert, sabreurs bleu).

  - La carte du joueur, (Celle qui s'appelle MOI juste au-dessus) sera aussi instanciée, mais aura une classe propre. Elle fera appel à des attributs et méthodes statiques des autres classes, par exemple pour récupérer la couleur, elle fera Mage.couleur.

  - Lorsqu'on survole un autre perso, sa carte se met en surbrillance, le curseur change, (et un message apparait : "Attaquer")

  - Une fenêtre popup apparait si on est mort, ou si on a tué quelqu'un avec le message approprié dedans.
