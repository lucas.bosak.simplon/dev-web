<?php
// Création de notre classe personnage
// Bien lire la syntaxe, on n'a pas les choses à l'identique de javascript.
class Personnage{
  // Un attribut public peut être accessible depuis l'extérieur de l'objet.
  public $nom = "Jean";
  // Un attribut private ne peut être utilisé qu'à l'intérieur de la classe (on ne peut pas l'appeler depuis l'extérieur).
  private $_vie = 100;
  private $_force;

  // un élément static est lié à la classe elle-même, et non aux objets qui sont créés. On peut l'appeler sans instancier la classe en faisant Nomdelaclass::elementstatic
  private static $_CrideMelee = "Yaaaaahhaaaaa <br>";

  private $CrideGuerre;

  private static $_nbPersonnages = 0;

  // Les constantes de classe sont des constantes liées à la classe, un petit peu comme les attributs et méthodes statiques. Une constante est définie une fois, puis on ne peut plus la changer. Elle permet de rendre lisible une valeur qui n'aurait pas de sens sinon.
  const PETITE_FORCE = 20;
  const MOYENNE_FORCE = 50;
  const GRANDE_FORCE = 80;
  public static $force_exeptionnelle = 150;

  // Comme en JS, on construit l'objet grâce à une méthode particulière, appelable qu'une seule fois : le constructeur.
  // Attention, pour les méthodes, ne pas oublier le mot function, contrairement à JS où il n'est pas utile.
  public function __construct($force){

    // $this fait référence à l'objet en cours.
    // on utilise ensuite les attributs ou méthodes de l'objet avec ->
    $this->_force = $force;

    // self (sans $ !!!) fait référence à la classe
    // On utilise ensuite les attributs ou méthodes de la classe avec ::
    self::setNbPersonnages();
  }

  // Les méthodes set et get, comme en JS permettent de lire et modifier des choses dans l'objet. Contrairement à JS, le mot set et get sont liés au nom de la fonction. Ce sont des conventions de lecture, il n'influent pas sur le fonctionnement du code.
  public function setForce($force){
    if ($force !== null) {
      $this->_force = $force;
    } else {
      $this->_force = 0;
    }
  }

  public function getForce(){
    return $this->_force;
  }

  // Une méthode static peut être appelée par tous les objet et la classe elle-même.
  public static function getCriDeMelee(){
    return self::$_CrideMelee;
  }
  public function getCriDeGuerre(){
    return $this->CrideGuerre;
  }

  public function setCriDeGuerre($nouveaucri){
    $this->CrideGuerre = $nouveaucri;
  }

  public static function getNbPersonnages(){
    return self::$_nbPersonnages;
  }

  // Une méthode private ne peut être appelée que depuis l'intérieur de la classe.
  private static function setNbPersonnages(){
    self::$_nbPersonnages ++;
  }
}
