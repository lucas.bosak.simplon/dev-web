<?php
// Cette fonction permet de charger les différents fichiers de classes qui seront requis pour l'exécution du code.
// IMPORTANT : vous devez avoir autant de fichiers que de classes, et les fichiers devront avoir le même nom que vos classes.
function chargerClasse($classe){
  require "classes/$classe.php";
}
// Cette fonction, interne à php, permet de lancer les classes seulement lorsqu'on les instancie. Ainsi, même si vous avez des milliers de classes, seules celles qui sont vraiment utilisées seront chargées.
// Pour que spl_autoload_register fonctionne, on lui passe le nom de la fonction à exécution comme une chaine de caractère.
// https://www.php.net/manual/fr/function.spl-autoload-register.php
spl_autoload_register('chargerClasse');


// Instanciation des personnages :
// On passe en paramètre une constante de classe, ou un attribut statique.

$Jean = new Personnage(Personnage::PETITE_FORCE);
$Adele = new Personnage(Personnage::GRANDE_FORCE);
$Mickey = new Personnage(Personnage::$force_exeptionnelle);

// Pour accéder à une propriété d'un objet, on utilise la flèche, et on enlève le $ devant l'attribut.
echo $Jean->nom;

var_dump($Jean->getForce());
$Jean->setForce(18);
var_dump($Jean->getForce());
var_dump($Adele->getForce());
var_dump($Mickey->getForce());

// Pour accéder à des attributs ou méthodes statiques (liées à la classe elle-même), on utilise les ::
echo $Jean::getCriDeMelee();
echo $Adele::getCriDeMelee();
echo $Mickey::getCriDeMelee();

$Jean->setCriDeGuerre('Moi je crie différemment ! <br>');

echo $Jean->getCriDeGuerre();
echo $Adele->getCriDeGuerre();
echo $Mickey->getCriDeGuerre();

// On peut tout à fait demander à la classe directement de nous renvoyer des attributs ou méthodes statiques, ou des constantes de classes.
echo Personnage::getCriDeMelee();

echo Personnage::getNbPersonnages() . "<br>";


// Pour faire beaucoup de classes d'un coup, plutôt que de tout déclarer à la main, on peut évidemment utiliser des boucles.
for ($i=0; $i < 100; $i++) {
  $soldats[$i] = new Personnage(Personnage::PETITE_FORCE);
  $soldats[$i]->setCriDeGuerre("Pour le Roi !! <br>");
}

echo Personnage::getNbPersonnages();

// Puis appeler un objet en particulier, pour lui demander de faire quelque chose en particulier.
$soldats[50]->setCriDeGuerre("Mamaaaaaaaan j'ai peur ! <br>");

// De cette manière, tous les soldats vont crier le cri de guerre, sauf un.
for ($i=0; $i < 100; $i++) {
  echo $soldats[$i]->getCriDeGuerre();
}
