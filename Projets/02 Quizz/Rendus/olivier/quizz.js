function son(){
    var son = new Audio();
    son.src = "clavier.mp3";
    son.play();
}

// Sélectionnez l'élément animé
const animatedElement = document.querySelector('instructions > p');

// Écoutez l'événement animationstart sur l'élément animé
animatedElement.addEventListener('animationstart', function() {
    son(); // Appelez la fonction son
    console.log('L\'animation a commencé !');
  });

function changeBackground() {
    document.body.classList.toggle(".body");
    document.body.classList.toggle(".snow");
    
  }