<?php
session_start();



if (isset($_POST['input'])) {
    if ($_POST['input'] === 'Luc Besson') {
        header('Location: cinemai5.php');
        exit;
    } else if ($_POST['input'] === 'Quentin Tarantino') {
        $_SESSION['scorei']++;
        header('Location: cinemai5.php');
        exit;
    } else if ($_POST['input'] === 'Samuel L. Jackson') {
        header('Location: cinemai5.php');
        exit;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="qt">
    <img class="pop" src="src/pop.png">
    <p class="tqt">Qui est ce réalisateur ?</p>
    <form method="post">
        <input class="cinema3" type="submit" name="input" value="Luc Besson">
        <input class="cinema3" type="submit" name="input" value="Samuel L. Jackson">
        <input class="cinema3" type="submit" name="input" value="Quentin Tarantino">
    </form>
</body>
</html>