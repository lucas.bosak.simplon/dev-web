<?php
session_start();



if (isset($_POST['input'])) {
    if ($_POST['input'] === 'PULP FICTION') {
        $_SESSION['score']++;
        header('Location: cinema5.php');
        exit;
    } else if ($_POST['input'] === 'GRAN TORINO') {
        header('Location: cinema5.php');
        exit;
    } else if ($_POST['input'] === 'RESERVOIR DOGS') {
        header('Location: cinema5.php');
        exit;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="Q4">
    <img class="pop" src="src/pop.png">
    <video id="ecran" autoplay controls>
        <source src="src/pf.mp4" type="video/mp4">
    </video>
    <p class="txtpf">De quel film est tirée cette scène ?</p>
    <form method="post">
        <input class="cinemapf" type="submit" name="input" value="PULP FICTION">
        <input class="cinemapf" type="submit" name="input" value="GRAN TORINO">
        <input class="cinemapf" type="submit" name="input" value="RESERVOIR DOGS">
    </form>
</body>
</html>