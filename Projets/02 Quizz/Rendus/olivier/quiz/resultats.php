<?php
session_start();


// ici je calcule le résultats des deux variables score et scores pour avoir une note sur 20
if ($_SESSION['scores'] === 5) {
    echo '<p class="pp1">Bravo vous avez fais un sans faute ! Votre score pour cette série est de 5/5, au total vous avez une moyenne de ' . ($_SESSION['score'] + ($_SESSION['scores']*2)) . '/20</p>';
} elseif ($_SESSION['scores'] === 4) {
    echo '<p class="pp1">Vous y étiez presque ! Votre score pour cette série est de 4/5, au total vous avez une moyenne de ' . ($_SESSION['score'] + ($_SESSION['scores']*2)) . '/20</p>';
} elseif ($_SESSION['scores'] === 3) {
    echo '<p class="pp1">Plutôt bien. Votre score pour cette série est de 3/5, au total vous avez une moyenne de ' . ($_SESSION['score'] + ($_SESSION['scores']*2)) . '/20</p>';
} elseif ($_SESSION['scores'] === 2) {
    echo '<p class="pp1">Dommage. Votre score pour cette série est de 2/5, au total vous avez une moyenne de ' . ($_SESSION['score'] + ($_SESSION['scores']*2)) . '/20</p>';
} elseif ($_SESSION['scores'] === 1) {
    echo '<p class="pp1">Pas si bon que ça. Votre score pour cette série est de 1/5, au total vous avez une moyenne de ' . ($_SESSION['score'] + ($_SESSION['scores']*2)) . '/20</p>';
} elseif ($_SESSION['scores'] === 0) {
    echo '<p class="pp1" style="color:red">Vous aurez plus de chance la prochaine fois ! Votre score pour cette série est de 0/5, au total vous avez une moyenne de ' . ($_SESSION['score'] + ($_SESSION['scores']*2)) . '/20</p>';
}

if (isset($_POST['input'])) {
    if ($_POST['input'] === 'ACCUEIL') {
        header('Location: quiz.php');
        exit;
    }
}

    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="p1">
    <img class="pop" src="src/pop.png">
    <form method="post">
        <input class="cinemaccueil" type="submit" name="input" value="ACCUEIL">
    </form>
</body>
</html>