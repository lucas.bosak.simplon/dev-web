<?php
session_start();



if (isset($_POST['input'])) {
    if ($_POST['input'] === 'Star Wars') {
        header('Location: resultati.php');
        exit;
    } else if ($_POST['input'] === 'Pirates des Caraïbes') {
        $_SESSION['scorei']++;
        header('Location: resultati.php');
        exit;
    } else if ($_POST['input'] === 'Charlie et la chocolaterie') {
        header('Location: resultati.php');
        exit;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="p4">
    <img class="pop" src="src/pop.png">
    <audio class="song" autoplay controls>
        <source src="src/pirate.mp3" type="audio/mpeg">
    </audio>
    <p class="txt2">De quel film est tirée cette musique ?</p>
    <form method="post">
        <input class="cinemapirate" type="submit" name="input" value="Pirates des Caraïbes">
        <input class="cinemapirate" type="submit" name="input" value="Star Wars">
        <input class="cinemapirate" type="submit" name="input" value="Charlie et la chocolaterie">
    </form>
</body>
</html>