<?php
session_start();



if (isset($_POST['input'])) {
    if ($_POST['input'] === 'Agent Z') {
        header('Location: cinemas2.php');
        exit;
    } else if ($_POST['input'] === 'Agent V') {
        header('Location: cinemas2.php');
        exit;
    } else if ($_POST['input'] === 'Agent K') {
        $_SESSION['scores']++;
        header('Location: cinemas2.php');
        exit;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="mib">
    <img class="pop" src="src/pop.png">
    <p class="txtmib">Dans le film Men In Black, comment s'appelle l'agent qui fait équipe avec Will Smith ?</p>
    <form method="post">
        <input class="cinema1" type="submit" name="input" value="Agent Z">
        <input class="cinema1" type="submit" name="input" value="Agent V">
        <input class="cinema1" type="submit" name="input" value="Agent K">
    </form>
</body>
</html>