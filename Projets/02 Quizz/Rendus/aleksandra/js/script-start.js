const quiz = document.querySelector('.quiz-body'); 
const btnStart = document.querySelector('.btn-start'); 

// hiding all the blocks of form
quiz.style.display = "none";

// showing the block of quiz after clicking the start button
btnStart.addEventListener('click', () => {
    quiz.style.display = "block";
});