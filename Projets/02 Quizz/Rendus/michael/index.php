<?php
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="Accueil.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>QUIZ - Machine learning</title>
</head>
<body>
<div class="bg_purple">   
    <div class="box1">
        <p class="titre1">Qu'est-ce que le machine learning ?</p>
        <p class="contenu1">Le Machine Learning peut être défini comme étant une technologie d'intelligence artificielle permettant aux machines d'apprendre sans avoir été au préalablement programmées spécifiquement à cet effet.<br> Le Machine Learning est explicitement lié au Big Data, étant donné que pour apprendre et se développer, les ordinateurs ont besoin de flux de données à analyser, sur lesquelles s'entraîner.</p>
    </div>
</div>
<div class="big_box2">
<div class="box2">
    <p class="quiz">Un quiz sur le machine learning t'intéresse ?</p>
</div>
</div>

<form action="pallier1.php" method="get" class="box_choix">
            <input name="oui" type="submit" value="Oui" class="button1">
            <input name="oui" type="submit" value="Oui" class="button2">
</form>
</body>
</html>