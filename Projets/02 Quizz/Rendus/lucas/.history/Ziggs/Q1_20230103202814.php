<?php 
require 'ziggs.php';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="stylesheet" href="../assets/css/style.css">
 <title>Q-Ziggs</title>
</head>
<body class="Q-Ziggs text-center">
<form method="POST" action="ziggs.php">  
<div class="Ques1" id="Quest1">
      <h1 class="Z-Q-h1 col-12">Question 1</h1>
      <p class="Z-Q-p col-12">Ou la demonstration de son talen as eu lieu.<?= $score ?></p>
        <div class="col dq">
            <input type="button" name="question1" id="question1" value="Yordle" class="btn btn-warning btn-lg R1" onclick="pass_suivant('Qpass','QAdd')">
            <input type="button" name="question1" id="question1" value="Piltover" class="btn btn-warning btn-lg R2" onclick="pass_suivant('Qpass','QAdd')"> 
            <input type="button" name="question1" id="question1" value="Bandle" class="btn btn-warning btn-lg R3" onclick="pass_suivant('Qpass','QAdd')">
        </div>
</div> 
<div class="Ques2" id="Quest2">
  <h1 class="Z-Q-h1 col-12">Question 2</h1>
  <p class="Z-Q-p col-12">Qui et l'expert des hexplosifs.<?= $score ?></p>
    <div class="col dq">
        <input type="button" name="question2" value="Ziggs" class="btn btn-warning btn-lg R1" id="Q2R1" onclick="pass_suivant2('QPass2','QAdd2')">
        <input type="button" name="question2" value="Hemerdinger" class="btn btn-warning btn-lg R2" id="Q2R2" onclick="pass_suivant2('QPass2','QAdd2')">
        <input type="button" name="question2" value="Fizz" class="btn btn-warning btn-lg R3" id="Q2R3" onclick="pass_suivant2('QPass2','QAdd2')">
      </div>
</div>                  
<div class="Ques3" id="Quest3">
  <h1 class="Z-Q-h1 col-12">Question 3</h1>
  <p class="Z-Q-p col-12">D'ou vien le commando qui capture les les professeurs.</p>
    <div class="col dq">
      <input type="button" name="question3" value="Bandle" class="btn btn-warning btn-lg R1" onclick="pass_suivant3('Ques3','Ques4')">
      <input type="button" name="question3" value="Runettra" class="btn btn-warning btn-lg R2" onclick="pass_suivant3('Ques3','Ques4')">
      <input type="button" name="question3" value="Zaun" class="btn btn-warning btn-lg R3" onclick="pass_suivant3('Ques3','Ques4')">
    </div>
</div>

<div class="Ques4" id="Quest4">
  <h1 class="Z-Q-h1 col-12">Question 4</h1>
  <p class="Z-Q-p col-12">Quelle et la couleur de la bombe de ziggs qui se trouve sur sont dos.</p>
    <div class="col dq">
      <input type="button" name="question4" value="Noir" class="btn btn-warning btn-lg R1" id="Q4R1" onclick="pass_suivant4('Ques4','Final')">
      <input type="button" name="question4" value="Rouge" class="btn btn-warning btn-lg R2" id="Q4R2" onclick="pass_suivant4('Ques4','Final')">
      <input type="button" name="question4" value="Blanc" class="btn btn-warning btn-lg R3" id="Q4R4" onclick="pass_suivant4('Ques4','Final')">
</div>
<?php 
</div>
<div class="Final" id="Final">
  <h1 class="Z-Q-h1 col-12">Merci</h1>
  <p class="Z-Q-p col-12">Ton score et de <?= $score ?>/4</p>
    <div class="col dq">
      <div class="contener confirm">
        <a href="../index.html"><button type="button" class="btn btn-warning btn-lg R3" id="Final" onclick="pass_suivant4('Ques4','Ques4')">Retour au menu</button></a>
      </div>
    </div>
</div>
</form>
<script src="javaS.js"></script>
 </body>
</html>