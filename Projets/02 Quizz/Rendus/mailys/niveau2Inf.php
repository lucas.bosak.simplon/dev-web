<?php
session_start();
include( 'config.php');
require 'functions.php';
verifConnection();
$score = $_SESSION['score'];
include('traitementN2I.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <title>Niveau 2 rattrapage</title>
</head>
<body class="bg-image img_monkey2">
    <!-- en tête de la page -->
    <header class="container-fluid bg-primary p-2 ">
        <h1 class="text-light text-center">Niveau 2 de rattrapage Monkey Island</h1>
        <h2> A toi de jouer <?= $_SESSION['username']; ?>!</h2>
        <!-- bouton retour page d'accueil -->
        <form method="POST">
         <button type="submit" class="bg-secondary text-light rounded col-xl-1 col-xs-5 " name="sortir" >sortir </button>   
        </form>
    </header>
    <form method="POST">               
            
             <!-- //division question1 -->
            <div class="container col-xl-6 col-sm-9 bg-tercery text-center p-5 m-auto mt-5 rounded-5 " id="q1_n2i">
                <label for="question1">Comment doit faire Guybrush pour sauver le prisonnier? </label><br>
                <input type="radio" name="reponseq1" id="q1reponse1"  value="q1reponse1">
                <label for="q1reponse1">Endormir un garde et voler les clefs.</label><br>
                <input type="radio" name="reponseq1" id="q1reponse2" value="q1reponse2">
                <label for="q1reponse2">Utiliser la magie Vaudou.</label><br>
                <input type="radio" name="reponseq1" id="q1reponse3" value="q1reponse3">
                <label for="q1reponse3">Faire fondre la serrure avec du grog.</label><br>
                <!-- bouton de validation -->
                <button class="bg-primary rounded p-1 mt-4" onclick="changerQuestion('q1_n2i', 'q2_n2i')"> question suivante</button>
        </div>
        <!-- bloc questionnaire deuxième question du niveau 1 -->
        <div class="container col-xl-6 col-sm-9 bg-tercery text-center p-5 m-auto mt-5 rounded-5  " style="display:none" id="q2_n2i">
            <!-- formulaire question réponses à cocher 1 seule réponse possible -->

                <label for="question2">Comment Guybrush accède au palais du gouverneur gardé par des chiens féroces?</label><br>
                <input type="radio"  name="reponseq2" id="q2reponse1"value="q2reponse1">
                <label for="q2reponse1">Il les tues à l'aide de son épée.</label><br>
                <input type="radio" name="reponseq2" id="q2reponse2" value="q2reponse2">
                <label for="q2reponse2">il les endort ave de la viande empoisonnée.</label><br>
                <input type="radio" name="reponseq2" id="q2reponse3" value="q2reponse3">
                <label for="q2reponse3">il utilise une poule pour faire diversion.</label><br>
                <!-- bouton de validation -->
                <button type="button" class="bg-primary rounded p-1 mt-4" onclick="changerQuestion('q2_n2i', 'q3_n2i')">question suivante </button>
                           <!-- bouton retour question precedante -->
            <button type="button" class="bg-secondary text-light mt-2" onclick="questionPrecedante('q2_n2i', 'q1_n2i')" >retour question précédante </button>
        </div>

        <div class="container col-xl-6 col-sm-9 bg-tercery text-center p-5 m-auto mt-5 rounded-5 " style="display:none" id="q3_n2i">
            <!-- formulaire question réponses à cocher 1 seule réponse possible -->          
                <label for="question3">A l'aide de quoi Guybrush effectue une tyrolienne?</label><br>
                <input type="radio" name="reponseq3" id="q3reponse1" value="q3reponse1">
                <label for="q3reponse1">Un poulet en plastique</label><br>
                <input type="radio" name="reponseq3" id="q3reponse2" value="q3reponse2">
                <label for="q3reponse2">Une chaine en métal</label><br>
                <input type="radio" name="reponseq3" id="q3reponse3" value="q3reponse3">
                <label for="q3reponse3">Une laisse de chien</label><br>
                <input type="submit" value="valider les réponses" name="formulaire2I" class="bg-primary rounded p-1 mt-4"id="btn_valide_N2I"><br>
            <button type="button" class="bg-secondary text-light mt-2" onclick="questionPrecedante('q3_n2i', 'q2_n2i')" >retour question précédante </button>
        </div>
    </form>
   
    <div class="etoile">
            <div class="content">    
                <h2>score:</h2>
                <p><?= $_SESSION['score'] ?> points</p>
            </div>
        </div>
     <script src="assets/script/javascript.js"> </script>
</body>
</html>