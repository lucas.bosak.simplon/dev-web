<?php session_start() ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/svg+xml" />
    <title>Resultat du Quiz</title>
    <link rel="stylesheet" href="./assets/css/main.css">
</head>

<body>
    <!-- Je dois trouver la commande pour cacher les warnings, si l'utilisateur ne choisir pas une réponse -->
    <div id="container_result">
        <h1>Votre resultat</h1>
        <?php

        $reponse6 = $_POST['question-6'];
        $reponse7 = $_POST['question-7'];
        $reponse8 = $_POST['question-8'];
        $reponse9 = $_POST['question-9'];
        $reponse10 = $_POST['question-10'];

        $Points2 = 0;

        if ($reponse6 == "C") {
            $Points2++;
        }
        if ($reponse7 == "D") {
            $Points2++;
        }
        if ($reponse8 == "A") {
            $Points2++;
        }
        if ($reponse9 == "B") {
            $Points2++;
        }
        if ($reponse10 == "D") {
            $Points2++;
        }

        $Points_2 = ($Points2 * 10);
        echo "<div id='results'><h3>Vous avez $Points_2&nbsp;points sur les questions de&nbsp;6&nbsp;à&nbsp;10.</h3></div>";
        $_SESSION["points2"] = $Points_2;
        ?>
    </div>

    <h2>Pour Continuer avec les questions de 11&nbsp;à&nbsp;15</h2>
    <a class="next_one" href="./quiz_3.php">
        <h3><span>Cliquez ICI</span></h3>
    </a>
</body>

</html>