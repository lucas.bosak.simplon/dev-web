<?php
session_start();
// var_dump($_SESSION["points1"])
?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/svg+xml" />
  <title>Questions de 6 à 10</title>
  <link rel="stylesheet" href="./assets/css/main.css" />
</head>

<body>
  <main>
    <div id="container">
      <header>
        <h1>Quiz Géographie</h1>
        <h2>Sur les capitales du&nbsp;monde</h2>
      </header>

      <button onclick="question_6();" class="start_game2" id="start_game2">
        Les questions de 6 à 10
      </button>
      <button class="btnstyle" id="btnQuest7" onclick="condensee('btnQuest7','seventh_li','btnQuest8','eighth_li');">
        Prochaine question 7
      </button>
      <button class="btnstyle" id="btnQuest8" onclick="question_8();">
        Prochaine question 8
      </button>
      <button class="btnstyle" id="btnQuest9" onclick="question_9();">
        Prochaine question 9
      </button>
      <button class="btnstyle" id="btnQuest10" onclick="question_10();">
        Prochaine question 10
      </button>

      <form action="result2.php" method="post" id="quiz">
        <ul id="urli">
          <li id="sixth_li">
            <h3>La capitale du Portugal est<span>...</span></h3>

            <div>
              <input type="radio" name="question-6" id="question-6-A" value="A" checked />
              <label for="question-6-A">A) Lusaka</label>
            </div>

            <div>
              <input type="radio" name="question-6" id="question-6-B" value="B" />
              <label for="question-6-B">B) Ljubljana</label>
            </div>

            <div>
              <input type="radio" name="question-6" id="question-6-C" value="C" />
              <label for="question-6-C">C) Lisbonne</label>
            </div>

            <div>
              <input type="radio" name="question-6" id="question-6-D" value="D" />
              <label for="question-6-D">D) Podgorica</label>
            </div>
          </li>

          <li id="seventh_li">
            <h3>La capitale des Pays-Bas est<span>...</span></h3>

            <div>
              <input type="radio" name="question-7" id="question-7-A" value="A" checked />
              <label for="question-7-A">A) Amman</label>
            </div>

            <div>
              <input type="radio" name="question-7" id="question-7-B" value="B" />
              <label for="question-7-B">B) La Haye</label>
            </div>

            <div>
              <input type="radio" name="question-7" id="question-7-C" value="C" />
              <label for="question-7-C">C) Lilongwe</label>
            </div>

            <div>
              <input type="radio" name="question-7" id="question-7-D" value="D" />
              <label for="question-7-D">D) Amsterdam</label>
            </div>
          </li>

          <li id="eighth_li">
            <h3>La capitale de la France est<span>...</span></h3>

            <div>
              <input type="radio" name="question-8" id="question-8-A" value="A" checked />
              <label for="question-8-A">A) Paris</label>
            </div>

            <div>
              <input type="radio" name="question-8" id="question-8-B" value="B" />
              <label for="question-8-B">B) Bordeaux</label>
            </div>

            <div>
              <input type="radio" name="question-8" id="question-8-C" value="C" />
              <label for="question-8-C">C) Lyon</label>
            </div>

            <div>
              <input type="radio" name="question-8" id="question-8-D" value="D" />
              <label for="question-8-D">D) Marseille</label>
            </div>
          </li>

          <li id="nineth_li">
            <h3>La capitale de l'Espagne est<span>...</span></h3>

            <div>
              <input type="radio" name="question-9" id="question-9-A" value="A" checked />
              <label for="question-9-A">A) Port of Spain</label>
            </div>

            <div>
              <input type="radio" name="question-9" id="question-9-B" value="B" />
              <label for="question-9-B">B) Madrid</label>
            </div>

            <div>
              <input type="radio" name="question-9" id="question-9-C" value="C" />
              <label for="question-9-C">C) Caracas</label>
            </div>

            <div>
              <input type="radio" name="question-9" id="question-9-D" value="D" />
              <label for="question-9-D">D) Port Vila</label>
            </div>
          </li>

          <li id="tenth_li">
            <h3>La capitale de la Turquie est<span>...</span></h3>

            <div>
              <input type="radio" name="question-10" id="question-10-A" value="A" checked />
              <label for="question-10-A">A) Palikir</label>
            </div>

            <div>
              <input type="radio" name="question-10" id="question-10-B" value="B" />
              <label for="question-10-B">B) Istanbul</label>
            </div>

            <div>
              <input type="radio" name="question-10" id="question-10-C" value="C" />
              <label for="question-10-C">C) Islamabad</label>
            </div>

            <div>
              <input type="radio" name="question-10" id="question-10-D" value="D" />
              <label for="question-10-D">D) Ankara</label>
            </div>
          </li>
        </ul>

        <input type="submit" value="Confirmez" id="SUBMIT" />
      </form>
    </div>
  </main>
  <script src="./assets/js/script2.js"></script>
</body>

</html>
