// Cacher les question
var lequiz6 = document.querySelector('#sixth_li').style.display = "none";
var lequiz7 = document.querySelector('#seventh_li').style.display = "none";
var lequiz8 = document.querySelector('#eighth_li').style.display = "none";
var lequiz9 = document.querySelector('#nineth_li').style.display = "none";
var lequiz10 = document.querySelector('#tenth_li').style.display = "none";

// Cacher les boutons
var hideButton6 = document.querySelector("#btnQuest7").style.display = "none";
var hideButton7 = document.querySelector("#btnQuest8").style.display = "none";
var hideButton8 = document.querySelector("#btnQuest9").style.display = "none";
var hideButton9 = document.querySelector("#btnQuest10").style.display = "none";

var submit0 = document.querySelector("#SUBMIT").style.display = "none";



// La fonction est appeler par le bouton "start_game" et demande de la fonction 'btnQuest6()' => d'afficher la première question et le bouton 'btnQuest6'
// => et de cacher le bouton 'start_game2'
function question_6() {
    // Caché
    var hideButton = document.querySelector("#start_game2").style.display = "none";
    // Visible
    hideButton6 = document.querySelector("#btnQuest7").style.display = "block";
    lequiz6 = document.querySelector('#sixth_li').style.display = "block";
}

// La fonction est appeler par le bouton "btnQuest6" et demande de la fonction 'btnQuest7()' => d'afficher la deuxième question et le bouton 'btnQuest7'
// => et de cacher le bouton 'btnQuest6' et la première question
function question_7() {
    // Caché
    hideButton6 = document.querySelector("#btnQuest7").style.display = "none";
    lequiz6 = document.querySelector('#sixth_li').style.display = "none";
    // Visible
    hideButton7 = document.querySelector("#btnQuest8").style.display = "block";
    lequiz7 = document.querySelector('#seventh_li').style.display = "block";
}

// La fonction est appeler par le bouton "btnQuest7" et demande de la fonction 'btnQuest8()' => d'afficher la troixième question et le bouton 'btnQuest8'
// => et de cacher le bouton 'btnQuest7' et la deuxième question
function question_8() {
    // Caché
    hideButton7 = document.querySelector("#btnQuest8").style.display = "none";
    lequiz7 = document.querySelector('#seventh_li').style.display = "none";
    // Visible
    hideButton8 = document.querySelector("#btnQuest9").style.display = "block";
    lequiz8 = document.querySelector('#eighth_li').style.display = "block";
}

// La fonction est appeler par le bouton "btnQuest8" et demande de la fonction 'btnQuest9()' => d'afficher la quatrième question et le bouton 'btnQuest9'
// => et de cacher le bouton 'btnQuest8' et la troixième question
function question_9() {
    // Caché
    hideButton8 = document.querySelector("#btnQuest9").style.display = "none";
    lequiz8 = document.querySelector('#eighth_li').style.display = "none";
    // Visible
    hideButton9 = document.querySelector("#btnQuest10").style.display = "block";
    lequiz9 = document.querySelector('#nineth_li').style.display = "block";
}

// La fonction est appeler par le bouton "btnQuest9" et demande de la fonction 'btnQuest10()' => d'afficher la cinqième question et le bouton 'SUBMIT'
// => et de cacher le bouton 'btnQuest9' et la quartième question
function question_10() {
    // Caché
    hideButton9 = document.querySelector("#btnQuest10").style.display = "none";
    lequiz9 = document.querySelector('#nineth_li').style.display = "none";
    // Visible
    lequiz10 = document.querySelector('#tenth_li').style.display = "block";
    submit0 = document.querySelector("#SUBMIT").style.display = "block";
}

// Jusqu'à là c'est bon mais ce n'est pas "DRY" !!!-