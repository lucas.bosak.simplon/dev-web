const questions = [
    {
        question: "Quel célèbre dictateur dirigea l’URSS du milieu des années 1920 à 1953 ?",
        optionA: "Staline",
        optionB: "Molotov",
        optionC: "Trotski",
        optionD: "Lénine",
        correctOption: "optionA"
    },

    {
        question: "Combien de joueurs sont autorisés sur un terrain de football ?",
        optionA: "10 joueurs",
        optionB: "11 joueurs",
        optionC: "9 joueurs",
        optionD: "12 joueurs",
        correctOption: "optionB"
    },

    {
        question: "Qui a été le premier président des États-Unis ?",
        optionA: "Donald Trump",
        optionB: "Barack Obama",
        optionC: "Abraham Lincoln",
        optionD: "George Washington",
        correctOption: "optionD"
    },

    {
        question: "À qui doit-on la chanson « I Shot the Sheriff »?",
        optionA: "Johnny Haliday",
        optionB: "Mickael Jackson",
        optionC: "Bob Marley",
        optionD: "Justin bieber",
        correctOption: "optionC"
    },

    {
        question: "Combien d'heures peut-on trouver dans une journée?",
        optionA: "30 heures",
        optionB: "38 heures",
        optionC: "48 heures",
        optionD: "24 heures",
        correctOption: "optionD"
    },

    {
        question: "Quel est le plus long fleuve du monde ?",
        optionA: "River Nile",
        optionB: "Long River",
        optionC: "River Niger",
        optionD: "Lake Chad",
        correctOption: "optionA"
    },

    {
        question: "_____est le plus long fleuve du monde",
        optionA: "Oceanie",
        optionB: "Antarctique",
        optionC: "Amazone",
        optionD: " Mississippi ",
        correctOption: "optionC"
    },

    {
        question: "Quel pays est le plus grand du monde ?",
        optionA: "Russie",
        optionB: "Canada",
        optionC: "Africa",
        optionD: "Egypt",
        correctOption: "optionA"
    },

    {
        question: "Lequel de ces nombres est un nombre impair ?",
        optionA: "10",
        optionB: "12",
        optionC: "8",
        optionD: "11",
        correctOption: "optionD"
    },

    {
        question: `"Tu ne peux pas me voir" est un dicton populaire de`,
        optionA: "Eminem",
        optionB: "Bill Gates",
        optionC: "Chris Brown",
        optionD: "John Cena",
        correctOption: "optionD"
    },

    {
        question: "Où se trouve le bâtiment le plus haut du monde?",
        optionA: "Afrique",
        optionB: "Californie",
        optionC: "Dubai",
        optionD: "Italie",
        correctOption: "optionC"
    },

    {
        question: "Le plus long fleuve du Royaume-Uni est?",
        optionA: "River Severn",
        optionB: "River Mersey",
        optionC: "River Trent",
        optionD: "River Tweed",
        correctOption: "optionA"
    },


    {
        question: "Combien de dents permanentes un chien a-t-il ?",
        optionA: "38",
        optionB: "42",
        optionC: "40",
        optionD: "36",
        correctOption: "optionB"
    },

    {
        question: "Quelle équipe nationale a remporté la coupe du monde de football en 2018 ?",
        optionA: "England",
        optionB: "Brésil",
        optionC: "Allemagne",
        optionD: "France",
        correctOption: "optionD"
    },

    {
        question: "Dans quel État américain est né Donald Trump ?",
        optionA: "New York",
        optionB: "California",
        optionC: "New Jersey",
        optionD: "Los Angeles",
        correctOption: "optionA"
    },

    {
        question: "Combien d'états le Nigeria a-t-il ?",
        optionA: "24",
        optionB: "30",
        optionC: "36",
        optionD: "37",
        correctOption: "optionC"
    },

    {
        question: "____ est la capital du Nigeria ?",
        optionA: "Abuja",
        optionB: "Lagos",
        optionC: "Calabar",
        optionD: "Kano",
        correctOption: "optionA"
    },

    {
        question: "Los Angeles est également connue sous le nom de ?",
        optionA: "Angels City",
        optionB: "Shining city",
        optionC: "City of Angels",
        optionD: "Lost Angels",
        correctOption: "optionC"
    },

    {
        question: "Quelle est la capitale de l'Allemagne?",
        optionA: "Georgia",
        optionB: "Missouri",
        optionC: "Oklahoma",
        optionD: "Berlin",
        correctOption: "optionD"
    },

    {
        question: "Combien de côtés a un hexagone ?",
        optionA: "6",
        optionB: "7",
        optionC: "4",
        optionD: "5",
        correctOption: "optionA"
    },

    {
        question: "Combien de planètes sont actuellement dans le système solaire ?",
        optionA: "11",
        optionB: "7",
        optionC: "9",
        optionD: "8",
        correctOption: "optionD"
    },

    {
        question: "Quelle planète est la plus chaude ?",
        optionA: "Jupiter",
        optionB: "Mercur",
        optionC: "Terre",
        optionD: "Venus",
        correctOption: "optionB"
    },

    {
        question: "où se trouve le plus petit os du corps humain?",
        optionA: "Orteils",
        optionB: "Oreilles",
        optionC: "Doigts",
        optionD: "Nez",
        correctOption: "optionB"
    },

    {
        question: "Combien de cœurs a une pieuvre ?",
        optionA: "1",
        optionB: "2",
        optionC: "3",
        optionD: "4",
        correctOption: "optionC"
    },

    {
        question: "Combien de dents a un humain adulte ?",
        optionA: "28",
        optionB: "30",
        optionC: "32",
        optionD: "36",
        correctOption: "optionC"
    }

]


let shuffledQuestions = [] //Tableau vide pour contenir les questions sélectionnées mélangées

function handleQuestions() {

    while (shuffledQuestions.length <= 9) {
        const random = questions[Math.floor(Math.random() * questions.length)]
        if (!shuffledQuestions.includes(random)) {
            shuffledQuestions.push(random)
        }
    }
}


let questionNumber = 1
let playerScore = 0
let wrongAttempt = 0
let indexNumber = 0

// Fonction pour afficher la question suivante dans le tableau à dom
function NextQuestion(index) {
    handleQuestions()
    const currentQuestion = shuffledQuestions[index]
    document.getElementById("question-number").innerHTML = questionNumber
    document.getElementById("player-score").innerHTML = playerScore
    document.getElementById("display-question").innerHTML = currentQuestion.question;
    document.getElementById("option-one-label").innerHTML = currentQuestion.optionA;
    document.getElementById("option-two-label").innerHTML = currentQuestion.optionB;
    document.getElementById("option-three-label").innerHTML = currentQuestion.optionC;
    document.getElementById("option-four-label").innerHTML = currentQuestion.optionD;

}


function checkForAnswer() {
    const currentQuestion = shuffledQuestions[indexNumber] //gets current Question
    const currentQuestionAnswer = currentQuestion.correctOption //gets current Question's answer
    const options = document.getElementsByName("option"); //gets all elements in dom with name of 'option' (in this the radio inputs)
    let correctOption = null

    options.forEach((option) => {
        if (option.value === currentQuestionAnswer) {
            //get's correct's radio input with correct answer
            correctOption = option.labels[0].id
        }
    })

    //checking to make sure a radio input has been checked or an option being chosen
    if (options[0].checked === false && options[1].checked === false && options[2].checked === false && options[3].checked == false) {
        document.getElementById('option-modal').style.display = "flex"
    }

    //checking if checked radio button is same as answer
    options.forEach((option) => {
        if (option.checked === true && option.value === currentQuestionAnswer) {
            document.getElementById(correctOption).style.backgroundColor = "green"
            playerScore++
            indexNumber++
            //set to delay question number till when next question loads
            setTimeout(() => {
                questionNumber++
            }, 1000)
        }

        else if (option.checked && option.value !== currentQuestionAnswer) {
            const wrongLabelId = option.labels[0].id
            document.getElementById(wrongLabelId).style.backgroundColor = "red"
            document.getElementById(correctOption).style.backgroundColor = "green"
            wrongAttempt++
            indexNumber++
            //set to delay question number till when next question loads
            setTimeout(() => {
                questionNumber++
            }, 1000)
        }
    })
}



//called when the next button is called
function handleNextQuestion() {
    checkForAnswer()
    unCheckRadioButtons()
    //delays next question displaying for a second
    setTimeout(() => {
        if (indexNumber <= 9) {
            NextQuestion(indexNumber)
        }
        else {
            handleEndGame()
        }
        resetOptionBackground()
    }, 1000);
}

//sets options background back to null after display the right/wrong colors
function resetOptionBackground() {
    const options = document.getElementsByName("option");
    options.forEach((option) => {
        document.getElementById(option.labels[0].id).style.backgroundColor = ""
    })
}

// unchecking all radio buttons for next question(can be done with map or foreach loop also)
function unCheckRadioButtons() {
    const options = document.getElementsByName("option");
    for (let i = 0; i < options.length; i++) {
        options[i].checked = false;
    }
}

// function for when all questions being answered
function handleEndGame() {
    let remark = null
    let remarkColor = null

    // condition check for player remark and remark color
    if (playerScore <= 3) {
        remark = "Mauvaises notes, continuez à pratiquer."
        remarkColor = "red"
    }
    else if (playerScore >= 4 && playerScore < 7) {
        remark = "Notes moyennes, vous pouvez faire mieux."
        remarkColor = "orange"
    }
    else if (playerScore >= 7) {
        remark = "Excellent, continuez bon travail !"
        remarkColor = "green"
    }
    const playerGrade = (playerScore / 10) * 100

    //data to display to score board
    document.getElementById('remarks').innerHTML = remark
    document.getElementById('remarks').style.color = remarkColor
    document.getElementById('grade-percentage').innerHTML = playerGrade
    document.getElementById('wrong-answers').innerHTML = wrongAttempt
    document.getElementById('right-answers').innerHTML = playerScore
    document.getElementById('score-modal').style.display = "flex"

}

//closes score modal and resets game
function closeScoreModal() {
    questionNumber = 1
    playerScore = 0
    wrongAttempt = 0
    indexNumber = 0
    shuffledQuestions = []
    NextQuestion(indexNumber)
    document.getElementById('score-modal').style.display = "none"
}

//function to close warning modal
function closeOptionModal() {
    document.getElementById('option-modal').style.display = "none"
}
