Niveau 1 : 

- n°1 : Comment s'appelle le frère de Mario, le célèbre héros de Nintendo ? 

	/a)Wario /b)Luigi /c)Toad

	réponse B

- n°2 : De quel type d'animal est Sonic, le célèbre héros de Sega ? 

	/a)Souris /b)Renard /c)Hérisson

	réponse C

- n°3 : Si on vous dit : Franklin, Trevor, Michael, vous nous dites...

	/a)Call of Duty /b)Uncharted /c)GTA

	réponse C

- n°4 : Comment appelle t-on les Jeux de rôle en ligne massivement multijoueur ?
	
	/a)MMORPG /b)MNOPRG /c)RPGMNO

	réponse A

- n°5 : Snake est un jeu qui a été intégré de base sur les téléphones ...

	/a)Nokia /b)Iphone /c)Samsung

	réponse A


Niveau 2 :

- n°1 : Quel commande de triche vous fais obtenir 50.000 simflouz dans les sims ?

	/a)Rosebud /b)GiveMeMoney /c)Motherlode

	réponse C

- n°2 : Quel est sont les starter de Pokémon rouge feu :

	/a)Salamèche,Bulbizarre,Carapuce /b)Tortipouss,Ouisticram,Tiplouf /c)Arcko,Poussifeu,Gobou

	réponse A

- n°3 : Quelle est le Boss du Nether dans Minecraft ?

	/a)EnderDragon /b)Wither /c)Notch

	réponse B

- n°4 : Quel jeu vidéo fait apparaître Donkey Kong pour la première fois ?

	/a)DonkeyKong Country /b)DonkeyKong 64 /c)DonkeyKong Arcade

	réponse C

- n°5 :	Quel société a développé World of Warcraft :

	/a)Blizzard /b)Ubisoft /c)Gameloft

	réponse A

- n°6 : Comment s'apelle l'héroïne de The Last of Us :
	
	/a)Abby /b)Ellie /c)Jessie

	réponse B

- n°7 : Quel est le jeu le plus speedruné ?

	/a)Super Mario 64 /b)Dark Souls /c) The Legends of Zelda (ocarina of time)

	réponse A
	
///- n°8 : Quand a été crée Pac-Man ?

	/a)1983 /b)1980 /c)1991

	réponse B
	
Niveau 3 : 

- n°1 :Quel était le nom de Mario dans ses premières apparitions ?
	
	/a)Enzo /b)Jumpman /c)Dr.Mario
	
	réponse B

- n°2 : Quel est le joueur de foot le plus représenter sur les couvertures du jeu FIFA ?

	/a)Messi /b)Ronaldo CR7 /c)Wayne Rooney

	réponse C

///- n°3 :Quel assassin's creed est connu pour se passer à l'age d'or de la piraterie :
	
	/a)Assassin's creed BlackFlag /b)Assassin's creed Syndicate /c)Assassin's creed Origin

	réponse A

- n°4 : Qui est le créateur de Mario : 
 
	/a)Shigeru Miyamoto /b)Fusajirō Yamauchi /c)Hayao Nakayama
	
	réponse A

- n°5 : Qui est le créateur de Minecraft :

	/a)Mojang /b)Microsoft /c)Notch

	réponse C

- n°6 : Quel est le jeu vidéo le plus vendu de la firme Nintendo : 

	/a)Super Mario Bros /b)Mario Kart Wii /c)Wii Sports

	réponse C

- n°7 : Quel champion de league of legends ressemble à un minotaure :/a) /b) /c)

	/a)Teemo /b)Alistar /c)Nasus
	
	réponse B

- n°8 : En quel année l'équipe Vitality signe son titre de champion du monde sur le jeu Rocket League ?

	/a)2020 /b)2018 /c)2019

	réponse C

- n°9 :Qui est le joueur le pus titré sur league of legends :

	/a)Faker /b)Domingo /c)Wolf

	réponse A

- n°10 : Quel call of duty n'est jamais sorti :

	/a)Call of duty 2 /b)Call of duty avanced warfare 3 /c)Call of duty moderne warfare 3

	réponse B

- n°11 : Lequel de ses jeux d'horreur est sortie en 1996:

	/a)Resident Evil /b)Silent Hill /c)Slender :The Eight Pages

	réponse A

- n°12 : Quel est le jeu qui à cacher le tout premier easter egg :

	/a)The legends of Zelda /b)Adventure /c)Pac-Man

	réponse B


TOTAL : 24 questions.