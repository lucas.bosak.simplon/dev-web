<?php
    session_start();
    $_SESSION['x']=1;
    $_SESSION['i'] = 0;
    $_SESSION['scoreA']=0;
    $_SESSION['scoreE']=0;
    $_SESSION['reponse']=null;
    if (empty($_SESSION['pasCompris'])) {
        $_SESSION['pasCompris']=0;
    }
    if (empty($_SESSION['aideLire'])) {
        $_SESSION['aideLire']=0;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css\quiz.css">
  <title>Document</title>
</head>
<body>


<!-- HEADER -->

    <div class="div1">
<!-- LOGO  -->
            <img class="logoNav"src="images\logosims.png" alt="">
<!-- TITRE HEADER -->
            <h1 class="titreNav">QUIZZ CULTURE JEUX VIDEOS</h1>
<!-- BOUTON RESTART-->
              <a href="index.php" class="restartButton"><b>RESTART</b></a>

    </div>

<!-- TITRE DU LEVEL -->

    <div class="div2">
        <h2 class="titreLevel" ><br>Bienvenue à toi <br> Voici les règles du Quizz</h2>
        </div>

<!-- C'EST LE TABLEAU -->

    <div class="div3">
        <br>
        <br>
        <p class="question"<?php
        if ($_SESSION['aideLire']==1) {
            echo "style=\"font-size:300%;\"";
            $_SESSION['aideLire']=0;
        }
    ?>>Ce quiz comporte 3 niveaux.<br>Sur chaque niveau tu vas devoir répondre au question qui te seront posés.<br>Fais attention à bien sélectionner une réponse avant de valider celle-ci !<br><br>Si tu réponds juste, ton personnage (celui de gauche) assénera un coup à ton adversaire.<br>Si ta réponse est fausse alors c'est ton personnage qui se verra prendre un coup. <br><br>Pour passer au niveau suivant il te suffit de vaincre ton adversaire.<br> Si tu réussi tous les niveaux tu as gagné ! Dans le cas ou ton personnage se fait battre tu as perdu.<?php
        if ($_SESSION['pasCompris']==1) {
            echo "<br><br> Je te laisse un peu de temps pour relire les règles.";
            $_SESSION['pasCompris']=0;
        }?>
         <br><br><br><br></p></div>
    <div class="div4" >
        <form action="pagevalidation.php" method="POST">
            <div class="div1-1" style="display: block; text-align:center; width:100vw;">
                      <p class="question">As tu compris les règles ? <br><br></p>

                    <label for="1" id="buttonA-1">Oui</label>
                    <input style="display:none" type="radio" class="oui" name="question1" id="1" value="oui">

                    <label for="2" id="buttonB-1">Non</label>
                    <input style="display:none;" type="radio" class="non" name="question1" id="2" value="non">

                    <p class="question" id="laquestion1"><br><br>Suivant</p>

            </div>
            <div class="div2-1" style="display: none; text-align:center; width:100vw;">
                      <p class="question">Tu as besoin d'aide pour lire ?<br><br></p>

                    <label for="3" id="buttonA-2">Oui</label>
                    <input style="display:none;" type="radio" class="oui" name="question2" id="3" value="oui">

                    <label for="4" id="buttonB-2">Non</label>
                    <input  style="display:none;" type="radio" class="non" name="question2" id="4" value="non">

                    <p class="question" id="laquestion2" ><br><br>Suivant</p>
            </div>
            <div class="div3-1" style="display:none; text-align:center; width:100vw;">
                      <p class="question" >Mais est ce que tu es prêt ?<br><br></p>

                    <label for="5" id="buttonA-3">Oui</label>
                    <input style="display:none;" type="radio" class="oui" name="question3" id="5" value="oui">

                    <label for="6" id="buttonB-3">Non</label>
                    <input style="display:none;" type="radio" class="non" name="question3" id="6" value="non">
                    <br><br><br>
                    <input  class="valideButton" style="background-color: #6297d6; " type="submit" value="VALIDER">
          </div>
    </form>
    </div>

<script src="js\quiz.js" defer></script>
</html>
