<?php
session_start();
$tabQuestion=[
  "Quel était le nom de Mario dans ses premières apparitions ?",
  "Quel est le joueur de foot le plus représenter sur les couvertures du jeu FIFA ?",
  "Quel assassin's creed est connu pour se passer à l'age d'or de la piraterie :",
  "Qui est le créateur de Mario :",
  "Qui est le créateur de Minecraft :",
  "Quel est le jeu vidéo le plus vendu de la firme Nintendo :",
  "Quel champion de league of legends ressemble à un minotaure :",
  "En quel année l'équipe Vitality signe son titre de champion du monde sur le jeu Rocket League ?",
  "Qui est le joueur le plus titré sur league of legends :",
  "Lequel de ses jeux d'horreur est sortie en 1996:",
  "Quel est le jeu qui à cacher le tout premier easter egg :"];

  $tabReponseA = ["Enzo","Messi","AC BlackFlag","S. Miyamoto","Mojang","Super Mario Bros","Teemo","2020","Faker","Resident Evil","The legends of Zelda"];

  $tabReponseB = ["Jumpman","Ronaldo CR7","AC Syndicate","F. Yamauchi","Microsoft","Mario Kart Wii","Alistar","2018","Domingo","Silent Hill","Adventure"];

  $tabReponseC = ["Dr.Mario","Wayne Rooney","AC Origin","H. Nakayama","Notch","Wii Sports","Nasus","2019","Wolf","Slender","Pac-Man"];

  $tabreponselvl1 =["b","c","a","a","c","c","b","c","a","a","b"];
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css\quiz.css">
  <title>Document</title>
</head>
<body>
 <div class="parent">

<!-- HEADER -->

    <div class="div1">
<!-- LOGO  -->
            <img class="logoNav"src="images\logochampi.png" alt="">
<!-- TITRE HEADER -->
            <h1 class="titreNav">QUIZZ CULTURE JEUX VIDEOS</h1>
<!-- BOUTON RESTART-->
              <a href="index.php" class="restartButton"><b>RESTART</b></a>

    </div>

<!-- TITRE DU LEVEL -->

    <div class="div2">
        <h2 class="titreLevel">Level 3</h2>
        </div>

<!-- C'EST LE TABLEAU -->

    <div class="div3">
        <div class="div3_1">
            <img class="bgTableau"src="images\BG_lvl3.png" alt="image d'arrière plan de streetfighter">

            <h2 class="alertPhone"><img class="imgAlertPhone" src="images\alertePhone.png" alt=""><br><br>Veuillez mettre votre téléphone au format horizontale pour profiter au mieux du quiz.</h2>
        </div>
      <img class="barreLifeR"src="images\barre_life_<?php echo "".$_SESSION['scoreA']."" ?>_lvl3.png" alt="">
      <img class="barreLifeL"src="images\barre_life_<?php echo "".$_SESSION['scoreE']."" ?>.png" alt="">
      <img class="perso1" src="images\perso1_lvl1.gif" alt="Ryu">
      <img class="perso2" src="images\perso2_lvl3.gif" alt="Guile"style="transform: scaleX(-1); height:60%;">

    </div>

<!-- QUESTIONS -->

    <div class="div4">
      <?php
      echo "<p class=\"question\">Question ".$_SESSION['x']." : ".$tabQuestion[$_SESSION['x']-1]."</p>";

       ?>
    </div>

<!-- BOUTONS REPONSES -->

        <div class="div5">

          <form action="1intermediairelvl3.php" method="post">

              <input type="radio" name="boutonreponse" class="boutonreponse" id="button-a" value="a">
              <label for="button-a" id="buttonA"> <?php echo"A&nbsp;:&nbsp;".$tabReponseA[$_SESSION['x']-1]."" ?> </label>

              <input type="radio" name="boutonreponse" class="boutonreponse" id="button-b" value="b">
              <label for="button-b" id="buttonB" > <?php echo"B&nbsp;:&nbsp;".$tabReponseB[$_SESSION['x']-1]."" ?> </label>

              <input type="radio" name="boutonreponse" class="boutonreponse" id="button-c" value="c">
              <label for="button-c" id="buttonC"><?php echo"C&nbsp;:&nbsp;".$tabReponseC[$_SESSION['x']-1]."" ?></label>

        </div>

    <!-- BOUTON VALIDATION -->

        <div class="div6">

              <input type="submit" name="valideButton" class="valideButton" id="boutonValidation" value="VALIDER">

            </form>

        </div>

</div>
</body>
<script src="js\quiz.js" defer></script>
</html>
