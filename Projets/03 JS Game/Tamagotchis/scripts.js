// Classe Tamagotchi qui est générale
// Age : [oeuf,bebe,enfant,adulte]

class Tamagotchi {
  // Attributs :
  _age;
  _vie;
  _genre;
  _nom;

  // si le tamagotchi est reveillé, c'est true
  // s'il est endormi c'est false
  _etat = "éveillé";

  // Méthodes
  // Savoir si le Tamagotchi est réveillé ou endormi
  get etat(){
    return this._etat; // état du tamagochi : éveillé ou endormi
  }

  // Faire dormir ou réveiller mon tamagotchi
  set etat(etat){
    if (etat == "endors-toi") {
      this._etat = "endormi";
    } else if (etat == "reveille-toi"){
      this._etat = "éveillé";
    }
  }

  set presentation(nom){
    this._nom = nom
  }

  get presentation(){
    console.log("je m'appelle "+this._nom);
  }


}

let text = document.getElementById('text');
let btntext = document.getElementById('btntext');

let tama = new Tamagotchi;

btntext.addEventListener('click',enregistrer);

function enregistrer(){
  tama.presentation =  text.value;
}
